from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {"proj_list": project}
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    proj = Project.objects.get(id=id)
    context = {
        "proj": proj,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        project = ProjectForm(request.POST)
        if project.is_valid():
            project.save()
            return redirect("list_projects")
    else:
        project = ProjectForm()
    context = {
        "create_project": project,
    }
    return render(request, "projects/create.html", context)
