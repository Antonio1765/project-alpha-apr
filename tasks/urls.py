from django.urls import path
from tasks.views import create_task, assigned_tasks


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", assigned_tasks, name="show_my_tasks"),
]
