from django.shortcuts import render, redirect
from .models import Task
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        task = TaskForm(request.POST)
        if task.is_valid():
            task.save()
            return redirect("home")
    else:
        task = TaskForm()
    context = {
        "create_task": task,
    }
    return render(request, "tasks/create.html", context)


@login_required
def assigned_tasks(request):
    assigned = Task.objects.filter(assignee=request.user)
    context = {
        "assigned": assigned,
    }
    return render(request, "tasks/assigned.html", context)
